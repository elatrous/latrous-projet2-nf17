--Lister tous les véhicules dans une région

SELECT n.id id
FROM vVehicule v
JOIN public.noeud n
ON v.fk_noeud=n.id
JOIN public.position p
ON n.fk_longitude=p.longitude AND n.fk_latitude=p.latitude
JOIN public.commune c
ON p.fk_code_postal=c.code_postal
WHERE c.nom='compiegne';

--Lister toutes les communications liées à une véhicule ou à une station de base

-- version 1
SELECT *
FROM vCommunication;

--version 2
SELECT *
FROM vCommunication
WHERE fk_noeud_emetteur=3 OR fk_noeud_recepteur=3; --put an id here

--Trouver le véhicule le plus proche d'un certain type, par exemple, un camion de pompiers, véhicule SAMU, etc. ou un certain modèle : toutes les Citroën C5, ...

SELECT v.fk_noeud,POWER(n.fk_longitude - t.tlong,2) + POWER(n.fk_latitude - t.tlat,2) distance
FROM vVehicule v
JOIN public.noeud n
ON v.fk_noeud=n.id
CROSS JOIN (
  SELECT n.fk_longitude tlong,n.fk_latitude tlat
  FROM vVehicule v	
  JOIN public.noeud n
  ON v.fk_noeud=n.id
  JOIN public.vehicule_info i
  ON v.numero_immatriculation=i.numero_immatriculation
  WHERE i.modele='modele a' --put a modele
  ) t
WHERE POWER(n.fk_longitude - t.tlong,2) + POWER(n.fk_latitude - t.tlat,2) <> 0				--Change the distance formula if you want, make sure the
ORDER BY distance											--difference isn't 0.
LIMIT 1;

--Only voiture got type so you can't test on vehicule

SELECT v.fk_noeud,POWER(n.fk_longitude - t.tlong,2) + POWER(n.fk_latitude - t.tlat,2) distance
FROM vVehicule v
JOIN public.noeud n
ON v.fk_noeud=n.id
CROSS JOIN (
  SELECT n.fk_longitude tlong,n.fk_latitude tlat
  FROM vVoiture v
  JOIN public.noeud n
  ON v.fk_noeud=n.id
  WHERE v.type_voiture='ordinaire' --put a type
  ) t
WHERE POWER(n.fk_longitude - t.tlong,2) + POWER(n.fk_latitude - t.tlat,2) <> 0				--Change the distance formula if you want, make sure the
ORDER BY distance											--difference isn't 0.
LIMIT 1;

--Calculer les statistiques de passage de véhicules : par zone, par type, ...

SELECT c.nom region, COUNT(*) nombre
FROM vVehicule v
JOIN public.noeud n
ON v.fk_noeud=n.id
JOIN public.position p
ON n.fk_longitude=p.longitude AND n.fk_latitude=p.latitude
JOIN public.commune c
ON p.fk_code_postal=c.code_postal
GROUP BY c.nom;

--Supprimer de la base toutes les communications qui concernent un certain modèle du capteur (par exemple, dont le mal-fonctionnement a été démontré).

DELETE FROM public.communication com
USING
  public.noeud AS n,
  public.capteur AS c
WHERE
  com.fk_noeud_emetteur=n.id AND
  n.id=c.fk_noeud AND
  c.modele='model 1';

-- after json transformation 

DELETE FROM public.communication com
USING
  public.noeud AS n,
  json_array_elements(n.capteurs) AS c
WHERE
  com.fk_noeud_emetteur=n.id AND
  value ->> 'modele'='model 1';

--Supprimer de la base toutes les communications qui concernent une station de base particulière (modèle défaillant).

DELETE FROM public.communication com
WHERE com.fk_noeud_emetteur=3 OR com.fk_noeud_recepteur=3 ;

