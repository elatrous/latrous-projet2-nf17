# Note de clarification

## Fonctions intenes à la base de données  :

La base de données pour la gestion de trafic de communication entre véhicules et stations de base (Infrastructure) doit permettre la gestion des interractions entres ces composants ainsi que l'historisation des évenements produits , ayant été détecté au préalable .
 la sauvgarde des ces données permet de détecter le fonctionnement des Capteurs qui peuvent être défaillant et propage donc une information imprécise . En plus de monitorier les voitures et leurs déplacement trié par zones géographiques ,elle permet aussi de gérer le infrastructures .
 les information receuillies sont alors le sujet des communications qui comportent des information sur des évènement qui peuvent subvenir . 
<br/><br/>
## Livrable :
https://gitlab.utc.fr/elatrous/latrous-projet2-nf17
<br/><br/>
-	README: Comporte le nom du sujet et sa désigniation
-	NDC: Note de Clarification
-	MCD: Modèle conceptuel de données
-	MLD: Modèle logique de données
-	BDD : tables et vues, données de test, questions attendues (vues)
-	Il faut intégrer dans le projet : héritage, contraintes, composition, vues, requêtes statistiques (agrégats), transaction et optimisation.
<br/><br/>
## Fonctions de la base de données

Voici les fonctions qui sont attendues :

⇒ Côté Conducteur :
-   permettre le transfert de de l'information concernant un évènement résultant d'une communication vers une infrastructure .
-   permettre d'indiquer ses informations spécifiques durant une communication (position, type , marque , capacité, modele , numéro_immatriculation ...)
-   permettre d'indiquer les autres véhicules qu'il transporte ainsi que les capteur installé dans son véhicule . 
-	ajouter une voiture dans son inventaire si le conducteur conduit un camion .

⇒ Côté Agent de maitenance :
-   permettre d’indiquer la localisation des infrastructure et de déclarer les capteurs installées .
-   Supprimer toutes les communication qui conserne un certain modèle du capteur .
-   Supprimer toutes les communications qui conserne une station de base particulière .
-	permet à l'agent d'ajouter suprimer et tranférer des Capteurs d'un Noeud à un autre 

⇒ Côté Agent de service public :

-	departement public 
	-   lister tous les véhicules dans une région donnée
	-   trouver le véhicule le plus proche d'un certain type ou un certain modéle 
	-	avoir accès aux statiques de passage de véhicules .

-	sécurité public 
	-   générer des statistiques sur le passage de véhicules en fonction de ( zone, types, état ...) .   
	-	avoir accès aux statiques de passage de véhicules .


## Hypothèses

- On suppose que l'intervention du de l'agent de maintenance agit directement sur la base de données.
- On suppose qu'un capteur peut détecter n'importe quel evenement et le transmet à la suite d'une communication .
- On suppose qu'un capteur est identifiée par son numéro de série et son modèle .
- On suppose que les communes répertoriées sont francaises .
    
## Liste des Objets identifiés :

-	Noeud (Capteur, Commune, Position) ( héritage exclusif)
	-	Infrastructure
	-	Véhicule
	<br/><br/>

-	évènenement ( héritage exclusif)
	-	Accident  (Type voiture )
	-	Alert météo
	-	détection materieles
	-	traveaux routieres 
	<br/><br/>
-	Véhicule ( héritage non exclusif)
	-	moto
	-	voiture ( héritage complet)
		-	voiture-SAMU
		-	voiture_police
		-	voiture_pompier
	-	camion (transporte )
	<br/><br/>
-	Communication (type de communication)
	<br/><br/>

## Propriéte associé aux Objets  :
Voici les différents objets que comporte la base de données et leur propriétés :

⇒ Insfrastructure (sous-classe de noeud) : O
-   id {nombre} {unique}

⇒ Noeud: une classe abstraite
-   +ajouter_capteur() {void} (ajoute un capteur qui n'existe pas dans la table Capteur)
-	+transférer_capteur(N1 Capteur,N2 Capteur) {Capteur} (changer le noeud assosié au Capteur )
-	+supprimer_capteur() {void} (enlever le lien du capteur avec le noeud assosié )

⇒ TypeCommunication : Enum
-   un_à_un
-	un_à_plusieurs

⇒ Type Voiture  : Enum
-   Camion
-	Voiture
-	voiture_spéciale
-	moto

⇒ Véhicule : classe abstraite
-   numero_immatriculation {nombre}{key}       
-   marque {texte}    
-   modèle {texte} 
-   année de production {date} 

⇒ Voiture (sous-classe de Véhicule) : O

⇒ Voiture_SAMU (sous-classe de Voiture) : O
⇒ Voiture_police(sous-classe de Voiture) : O
⇒ Voiture_pompier (sous-classe de Voiture) : O


⇒ Camion  (sous-classe de Véhicule) : O
-	capcité_maximale {nombre} 

⇒ Moto  (sous-classe de Véhicule) : O
-	capcité_moto {nombre} 

⇒ Capteur : O
-	numéro_série : {nombre}
-	modèle {texte}

⇒ Commune : O
-   nom {texte}{key}    
-   code_postale {nombre}{unique}  

⇒ Position : O
-   longitude {nombre}{unique}   
-   latitude {nombre}{unique} 

⇒ Evènement : O
-	timestamp {date}
-	id_entite {nombre}
-	contenu {unique} {text}

⇒ accident (sous-classe de évènement ): O
-	timestamp {date}
-	id_entite {nombre}
-	contenu {unique} {text}

⇒ alert_météo (sous-classe de évènement ): O
-	temps {texte}
-	température {nombre}

⇒ detection_materiel (sous-classe de évènement ): O
⇒ traveau_routier  (sous-classe de évènement ): O

⇒ Communication : (Noeud - Noeud) 
-	type_de_communication : {TypeCommunication}

⇒ transporte : (Voiture - Camion) 
-   id_voiture {clé étrangère id_node dans Voiture}    
-   id_camion {clé étrangère id_node dans Camion}
<br/><br/>

## Propriéte associé aux Relations entre Objets :
⇒ Evènenement **se déroule** dans commune avec cardinalité (N:1)
<br/><br/>
- on devra s'assurer de la contrainte de cardinalité minimale de l'Evènenement assoocié à une commune donnée
<br/><br/>
⇒ Capteur **détecte** Evenement de cardinalité (1:N)
<br/><br/>
- on devra s'assurer de la contrainte de cardinalité minimale
<br/><br/>
⇒ Noeud **se trouve** dans une position 
<br/><br/>
-	l'infrastructure poséde une position immuable alors que le véhicule poséde une position qui est variable, on peut gérer cela avec une contrainte simple.
<br/><br/>
⇒ Position **appartient** à une et une seule Commune (selon l'hypothèse)avec Cardinalité (1:N) 
<br/><br/>
-	On devra gérer la contrainte de cardinalité minimale avec une contrainte simple.
<br/><br/>
⇒ Capteur est composé avec Noeud (1:1..N) dans le cas des infrastructures et (1:0..N) dans le cas des véhicules 
<br/><br/>
-	On devra gérer cela  à l'aide d'une vue qui permettra de s'assurer de la validité de la cardinalité selon le cas
<br/><br/>

⇒ Communication est composé **produit** avec Evènenement (1:1) 
<br/><br/>
-	On devra s'assurer de la condition minimale de cardinalité dans l'implémentation
<br/><br/>
⇒ Classe-association Communication entre deux Noeuds de cardinalité (1.. 1, max(1, 1..N)) 
<br/><br/>
-	On devra vérifier qu'une communication ne peut se faire que entre deux composants différents ( test sur la clé primaire)
