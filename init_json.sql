drop table IF EXISTS public.transport;
drop view IF EXISTS vCommunication;
drop table IF EXISTS public.communication;
drop type IF EXISTS type_communication;
drop view IF EXISTS vVehicule;
drop view IF EXISTS vVoiture;
drop table IF EXISTS public.voiture;
drop type IF EXISTS type_voiture_speciale;
drop view IF EXISTS vMoto;
drop table IF EXISTS public.moto;
drop view IF EXISTS vCamion;
drop table IF EXISTS public.camion;
drop table IF EXISTS public.vehicule_info;
drop view IF EXISTS vInfrastructure;
drop table IF EXISTS public.infrastructure;
drop table IF EXISTS public.noeud;
drop table IF EXISTS public.position;
drop table IF EXISTS public.commune;

-- Table: public.commune

-- DROP TABLE public.commune;

CREATE TABLE public.commune
(
  code_postal integer NOT NULL,
  nom varchar,
  PRIMARY KEY (code_postal)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.commune
  OWNER TO postgres;

-- Table: public.position

-- DROP TABLE public.position;

CREATE TABLE public.position
(
  longitude double precision NOT NULL,
  latitude double precision NOT NULL,
  fk_code_postal integer,
  PRIMARY KEY (longitude, latitude),
  FOREIGN KEY (fk_code_postal) REFERENCES public.commune (code_postal) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.position
  OWNER TO postgres;

-- Table: public.noeud

-- DROP TABLE public.noeud;

CREATE TABLE public.noeud
(
  id serial,
  fk_longitude double precision,
  fk_latitude double precision,
  capteurs JSON,
  PRIMARY KEY (id),
  FOREIGN KEY (fk_longitude, fk_latitude) REFERENCES public.position (longitude, latitude) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.noeud
  OWNER TO postgres;

-- Table: public.infrastructure

-- DROP TABLE public.infrastructure;

CREATE TABLE public.infrastructure
(
  id_i serial,
  fk_noeud integer,
  PRIMARY KEY (fk_noeud),
  FOREIGN KEY (fk_noeud) REFERENCES public.noeud (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE

)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.infrastructure
  OWNER TO postgres;

CREATE VIEW vInfrastructure AS
  SELECT *
  FROM public.infrastructure i
  JOIN public.noeud n
  ON i.fk_noeud=n.id;

-- Table: public.vehicule_info

-- DROP TABLE public.vehicule_info;

CREATE TABLE public.vehicule_info
(
  numero_immatriculation integer PRIMARY KEY,
  marque varchar,
  modele varchar,
  annee_production integer
);

-- Table: public.camion

-- DROP TABLE public.camion;

CREATE TABLE public.camion
(
  fk_noeud integer,
  numero_immatriculation integer UNIQUE,
  capacite_maximale integer,
  PRIMARY KEY (Fk_noeud),
  FOREIGN KEY (Fk_noeud) REFERENCES public.noeud (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (numero_immatriculation ) REFERENCES public.vehicule_info(numero_immatriculation) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE

)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.camion
  OWNER TO postgres;

CREATE VIEW vCamion AS
  SELECT *
  FROM public.camion c
  JOIN public.noeud n
  ON c.fk_noeud=n.id;

-- Table: public.moto

-- DROP TABLE public.moto;

CREATE TABLE public.moto
(
  fk_noeud integer,
  numero_immatriculation integer UNIQUE,
  capacite_motoe integer,
  PRIMARY KEY (fk_noeud),
  FOREIGN KEY (fk_noeud) REFERENCES public.noeud (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (numero_immatriculation ) REFERENCES public.vehicule_info(numero_immatriculation) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.moto
  OWNER TO postgres;

CREATE VIEW vMoto AS
  SELECT *
  FROM public.moto m
  JOIN public.noeud n
  ON m.fk_noeud=n.id;

-- Table: public.voiture

-- DROP TABLE public.voiture

CREATE TYPE type_voiture_speciale AS enum('ordinaire','voiture_SAMU','voiture_policier','voiture pompier');
CREATE TABLE public.voiture
(
  fk_noeud integer,
  numero_immatriculation integer UNIQUE,
  type_voiture type_voiture_speciale NOT NULL,
  PRIMARY KEY (fk_noeud),
  FOREIGN KEY (fk_noeud) REFERENCES public.noeud (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (numero_immatriculation ) REFERENCES public.vehicule_info(numero_immatriculation) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.voiture
  OWNER TO postgres;

CREATE VIEW vVoiture AS
  SELECT *
  FROM public.voiture v
  JOIN public.noeud n
  ON v.fk_noeud=n.id;

-- View: vVehicule

CREATE VIEW vVehicule AS
  SELECT fk_noeud,numero_immatriculation FROM vVoiture
  UNION
  SELECT fk_noeud,numero_immatriculation FROM vMoto
  UNION
  SELECT fk_noeud,numero_immatriculation FROM vCamion;

-- Table: public.communication

-- DROP TABLE public.communication;

CREATE TYPE type_communication AS enum('un', 'plusieur');
CREATE TABLE public.communication
(
  id_c serial,
  fk_noeud_emetteur integer NOT NULL,
  fk_noeud_recepteur integer NOT NULL,
  stamp date,
  type_communication type_communication NOT NULL,
  evenement json NOT NULL,
  PRIMARY KEY (id_c),
  FOREIGN KEY (fk_noeud_emetteur) REFERENCES public.noeud (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (fk_noeud_recepteur) REFERENCES public.noeud (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT communication_check_diff CHECK (fk_noeud_emetteur <> fk_noeud_recepteur)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.communication
  OWNER TO postgres;

CREATE VIEW vCommunication AS
  SELECT *,
    CASE WHEN fk_noeud_emetteur IN (SELECT fk_noeud FROM vVehicule) THEN 'un'
    ELSE 'vrai'
    END as "vrai_type"
  from public.communication;

-- Table: public.transport

-- DROP TABLE public.transport;

CREATE TABLE public.transport
(
  fk_voiture integer,
  fk_camion integer,
  PRIMARY KEY (fk_voiture,fk_camion),
  FOREIGN KEY (fk_camion) REFERENCES public.camion (fk_noeud) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (fk_voiture) REFERENCES public.voiture (fk_noeud) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.transport
  OWNER TO postgres;


-- Templates for the json

/* 
capteurs
[
  {
    id: ID
    serie: string
    modele: string
  },
  {
    id: ID
    serie: string
    modele: string
}
...
]

evenement
{
  capteur: ID,
  contenu: string,
  gravité: int,
  nombre_véhicules: int,
  types_véhicules: string,
  temperature: double,
  type: string,
}

*/




insert into public.commune (code_postal ,nom )  values (60200, 'compiegne');
insert into public.commune (code_postal ,nom )  values (60100, 'creil');
insert into public.commune (code_postal ,nom )  values (60300, 'senlis');
insert into public.commune (code_postal ,nom )  values (60000, 'beauvais');

insert into public.position (longitude,latitude,fk_code_postal)  values ( 2.8261 , 49.4179,60200);
insert into public.position (longitude,latitude,fk_code_postal)  values ( 2.0833, 49.4333,60200);
insert into public.position (longitude,latitude,fk_code_postal)  values ( 2.4833, 49.2667,60100);
insert into public.position (longitude,latitude,fk_code_postal)  values ( 2.5833, 49.2,60300);
insert into public.position (longitude,latitude,fk_code_postal)  values ( 2.7833, 49.4,60200);

insert into public.noeud (fk_longitude, fk_latitude, capteurs ) 
		   values (2.8261 , 49.4179,'[
			{"id": 1, "serie": "serie 1", "modele": "model 1"}
		   ]');

insert into public.noeud (fk_longitude, fk_latitude, capteurs )
		   values (2.0833, 49.4333,'[
			{"id": 1, "serie": "serie 7", "modele": "model 8"}
		   ]');
insert into public.noeud (fk_longitude, fk_latitude, capteurs )
		   values (2.4833, 49.2667,'[
			{"id": 1, "serie": "serie 3", "modele": "model 5"}
		   ]');
insert into public.noeud (fk_longitude ,fk_latitude, capteurs )
		   values (2.5833, 49.2, null);
insert into public.noeud (fk_longitude ,fk_latitude, capteurs )
		   values (2.7833, 49.4, null);

insert into public.infrastructure (id_i ,fk_noeud )  values (1, 1);
insert into public.infrastructure (id_i ,fk_noeud )  values (2, 3);

insert into public.vehicule_info ( numero_immatriculation, marque, modele, annee_production)
		   VALUES (10000,'marque a','modele a', 1970);
insert into public.vehicule_info (numero_immatriculation,  marque, modele, annee_production)
		   VALUES (10001,'marque b','modele b',1974);
insert into public.vehicule_info ( numero_immatriculation, marque, modele, annee_production)
		   VALUES (10002,'marque c','modele c',1954);

insert into public.camion (fk_noeud, numero_immatriculation, capacite_maximale)
		   values (2,10000,3);


insert into public.moto (fk_noeud, numero_immatriculation, capacite_motoe)
		   values (4,10001,2);

insert into public.voiture (fk_noeud, numero_immatriculation, type_voiture)
		   values (5,10002,'ordinaire');


insert into public.communication (fk_noeud_emetteur, fk_noeud_recepteur, stamp, type_communication, evenement)
		   values (1,3,'2001-09-28','un',
			'{"capteur": 1,"contenu": "warning", "gravité": null, "nombre_véhicules": null, "types_véhicules": null, "temperature": -20, "type": "alert_meteo"}'
		   );
insert into public.communication (fk_noeud_emetteur, fk_noeud_recepteur, stamp, type_communication, evenement)
		   values (3,1,'2001-09-28','plusieur',
			'{"capteur":1 ,"contenu": "accident", "gravité": 10, "nombre_véhicules": 2,"types_véhicules": "ordinaire", "temperature": null, "type": "accident"}'
		   );
insert into public.communication (fk_noeud_emetteur, fk_noeud_recepteur, stamp, type_communication, evenement)
		   values (3,5,'2001-09-28','plusieur',
			'{"capteur": 1,"contenu": "accident", "gravité": 10, "nombre_véhicules": 2,"types_véhicules": "ordinaire", "temperature": null, "type": "accident"}'
		   );

insert into public.transport (fk_voiture, fk_camion) values (5,2);


CREATE USER car_driver1;
CREATE USER moto_driver1;
CREATE GROUP Private_driver WITH USER car_driver1 , moto_driver1 ;
CREATE USER truck_driver1;
CREATE GROUP truck_driver WITH USER truck_driver1 ;
CREATE USER admin1;
CREATE GROUP ADMIN WITH USER admin1;

CREATE USER maintenance_agent1;

CREATE USER maintenance_agent2;
CREATE GROUP maintenance_agent WITH USER maintenance_agent1,
maintenance_agent2;


CREATE USER police_agent1;
CREATE GROUP public_department WITH USER 
police_agent1;
CREATE USER safety_agent1;
CREATE USER statistician1;

CREATE GROUP safety_department WITH USER safety_agent1, statistician1;



GRANT SELECT, UPDATE, INSERT, DELETE ON   moto, voiture TO Private_driver;
GRANT  SELECT ,INSERT ON communication TO car_driver1;

GRANT  SELECT ,UPDATE, INSERT, DELETE ON transport, camion  to truck_driver ;
GRANT  SELECT ,INSERT ON communication TO truck_driver;

GRANT SELECT, UPDATE, INSERT, DELETE ON infrastructure TO maintenance_agent;
GRANT DELETE ON communication TO maintenance_agent;

GRANT SELECT ON vcommunication , vmoto, vvehicule, vvoiture , vcamion, vinfrastructure TO public_department;
GRANT SELECT ON vcommunication , vmoto, vvehicule, vvoiture , vcamion TO safety_department;
GRANT SELECT ON  vinfrastructure TO safety_department;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO ADMIN;

